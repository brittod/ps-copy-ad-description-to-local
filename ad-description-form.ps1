﻿function collectInformation ($title,$hostsOrLabs,$topLvlOu, $subLvlOu, $subSubLvlOu) {

    ###################Load Assembly for creating form & button######

    [void][System.Reflection.Assembly]::LoadWithPartialName( “System.Windows.Forms”)
    [void][System.Reflection.Assembly]::LoadWithPartialName( “Microsoft.VisualBasic”)

    #####Define the form size & placement

    $form = New-Object “System.Windows.Forms.Form”;
    $form.Width = 600;
    $form.Height = 300;
    $form.Text = $title;
    $form.StartPosition = [System.Windows.Forms.FormStartPosition]::CenterScreen;

    ##############Define text label0
    $textLabel0 = New-Object "System.Windows.Forms.Label";
    $textLabel0.Left = 25;
    $textLabel0.Top = 10;

    $textLabel0.Text = $hostsOrLabs;

    ##############Define text label1
    $textLabel1 = New-Object “System.Windows.Forms.Label”;
    $textLabel1.Left = 25;
    $textLabel1.Top = 50;

    $textLabel1.Text = $topLvlOu;

    ##############Define text label2

    $textLabel2 = New-Object “System.Windows.Forms.Label”;
    $textLabel2.Left = 25;
    $textLabel2.Top = 85;

    $textLabel2.Text = $subLvlOu;

    ##############Define text label3

    $textLabel3 = New-Object “System.Windows.Forms.Label”;
    $textLabel3.Left = 25;
    $textLabel3.Top = 120;

    $textLabel3.Text = $subSubLvlOu;

    ############Define text box0 for input
    $textBox0 = New-Object “System.Windows.Forms.TextBox”;
    $textBox0.Left = 200;
    $textBox0.Top = 10;
    $textBox0.width = 200;

    ############Define text box1 for input
    $textBox1 = New-Object “System.Windows.Forms.TextBox”;
    $textBox1.Left = 200;
    $textBox1.Top = 50;
    $textBox1.width = 200;

    ############Define text box2 for input

    $textBox2 = New-Object “System.Windows.Forms.TextBox”;
    $textBox2.Left = 200;
    $textBox2.Top = 85;
    $textBox2.width = 200;

    ############Define text box3 for input

    $textBox3 = New-Object “System.Windows.Forms.TextBox”;
    $textBox3.Left = 200;
    $textBox3.Top = 120;
    $textBox3.width = 200;

    #############Define default values for the input boxes
    $defaultValue = “”
    $textBox0.Text = $defaultValue;
    $textBox1.Text = $defaultValue;
    $textBox2.Text = $defaultValue;
    $textBox3.Text = $defaultValue;

    #############define OK button
    $button = New-Object “System.Windows.Forms.Button”;
    $button.Left = 450;
    $button.Top = 120;
    $button.Width = 100;
    $button.Text = “Copy”;

    ############# This is when you have to close the form after getting values
    $eventHandler = [System.EventHandler]{
    $textBox0.Text;
    $textBox1.Text;
    $textBox2.Text;
    $textBox3.Text;
    $form.Close();};

    $button.Add_Click($eventHandler) ;

    #############Add controls to all the above objects defined
    $form.Controls.Add($button);
    $form.Controls.Add($textLabel0)
    $form.Controls.Add($textLabel1);
    $form.Controls.Add($textLabel2);
    $form.Controls.Add($textLabel3);
    $form.Controls.Add($textBox0);
    $form.Controls.Add($textBox1);
    $form.Controls.Add($textBox2);
    $form.Controls.Add($textBox3);
    $ret = $form.ShowDialog();

    #################return values

    return $textBox0.Text, $textBox1.Text, $textBox2.Text, $textBox3.Text
}

$activeDirectoryStructure = collectInformation “Enter OU Structure” "Enter 'Hosts' or 'Labs'" “Enter Top-Level OU” “Enter Sub-OU” "Enter Sub-sub-OU"

Get-Module ADComputer

$credential = Get-Credential

if($activeDirectoryStructure -contains "Hosts" -or "Labs"){
    if ([string]::IsNullOrEmpty($activeDirectoryStructure[3]) -and [string]::IsNullOrEmpty($activeDirectoryStructure[2])){
        $compOuString = "ou=" + $activeDirectoryStructure[1] + ",ou=" + $activeDirectoryStructure[0] + ",dc=univ,dc=dir,dc=wwu,dc=edu"
        $comps = Get-ADComputer -searchbase $compOuString -filter * -Properties Description
        ForEach ($comp in $comps) {
            $description = $comp.Description
            if([string]::IsNullOrEmpty($comp.Description)){
                $compName = $comp.name
                $compName | Out-File 'C:\Windows\Temp\CompDescriptionNeeded.txt' -Append
            }
            Invoke-Command -ComputerName $comp.Name -Credential $credential -ArgumentList $description -ScriptBlock { 
                param ($description) New-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters -Name srvComment -PropertyType string -Value $Description -Force
            }
        }
    }
    elseif ([string]::IsNullorEmpty($activeDirectoryStructure[3])){
        $subCompOuString = "ou=" + $activeDirectoryStructure[2] + ",ou=" + $activeDirectoryStructure[1] + ",ou=" + $activeDirectoryStructure[0] + ",dc=univ,dc=dir,dc=wwu,dc=edu"
        $subComps = Get-ADComputer -searchbase $subCompOuString -filter * -Properties Description
        ForEach ($comp in $subComps) {
            $description = $comp.Description
            if([string]::IsNullOrEmpty($comp.Description)){
                $compName = $comp.name
                $compName | Out-File 'C:\Windows\Temp\CompDescriptionNeeded.txt' -Append 
            }
            Invoke-Command -ComputerName $comp.Name -Credential $credential -ArgumentList $description -ScriptBlock { 
                param ($description) New-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters -Name srvComment -PropertyType string -Value $Description -Force
            }
        }
    }
    elseif ($activeDirectoryStructure){
        $subSubCompOuString = "ou=" + $activeDirectoryStructure[3] + ",ou=" + $activeDirectoryStructure[2] + ",ou=" + $activeDirectoryStructure[1] + ",ou=" + $activeDirectoryStructure[0] + ",dc=univ,dc=dir,dc=wwu,dc=edu"
        $subSubComps = Get-ADComputer -searchbase $subSubCompOuString -filter * -Properties Description
        ForEach ($comp in $subComps) {
            $description = $comp.Description
            if([string]::IsNullOrEmpty($comp.Description)){
                $compName = $comp.name
                $compName | Out-File 'C:\Windows\Temp\CompDescriptionNeeded.txt' -Append 
            }
            Invoke-Command -ComputerName $comp.Name -Credential $credential -ArgumentList $description -ScriptBlock { 
                param ($description) New-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters -Name srvComment -PropertyType string -Value $Description -Force
            }
        }
    }
    else {
        ##Tell user they have bad data
    }
}
else {
    ##Tell user they have bad data
}


Write-Host "You made it here."